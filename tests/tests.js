// in order to see the app running inside the QUnit runner
App.rootElement = '#ember-testing';

// Common test setup
App.setupForTesting();
App.injectTestHelpers();

// common QUnit module declaration
module("Integration tests", {
  setup: function() {
    // before each test, ensure the application is ready to run.
    Ember.run(App, App.advanceReadiness);
  },

  teardown: function() {
    // reset the application state between each test
    App.reset();
  }
});

// QUnit test case
test("Index Page displays the right header and questions", function() {
  // async helper telling the application to go to the '/' route
  visit("/");

  // helper waiting the application is idle before running the callback
  andThen(function() {
    equal(find("h2").text(), "Welcome to Emberoverflow", "Application header is rendered");
    notEqual(find("ul:not(.nav) > li").length, 0, "There are questions in the list");
  });

  //Testing the links to questions
  test("Question Link goes to question page and question is displayed", function(){
    visit("/");
    click("ul:not(.nav) > li > a:first");

    andThen(function(){
      equal(
        find("h2").length,
        2,
        "Question and Application Headers are rendered"
      );

      equal(
        find("p").length,
        3,
        "Question, edit link and Author are rendered"
      );
    })
  })

  test('Testing Sign in with an email', function(){
    delete localStorage['currentUser'];
    App.set('currentUser', undefined);

    visit('/sign-in');

    fillIn(".form-control", "rhistan@gmail.com");
    click("button");

    andThen(function(){
      equal(
        find("p").text(),
        "You are already signed in!",
        "Signed in message is rendered successully"
      )
    });
  });

  test("Singed in user can ask and answer questions", function(){
    localStorage['currentUser'] = 201;
    App.set('currentUser', 201);

    visit('/ask-question');
    fillIn('#topic', "Question topic");
    fillIn('#question', "Question text");

    click("button");

    fillIn('#answer', "Answer");

    click("button");

    andThen(function(){
      equal(
        find('h2:last').text(),
        'Question topic',
        "Question Topic is rendered"
      );

      equal(
        find('p#question').text(),
        'Question text',
        "Question Body is rendered"
      );

      notEqual(
        find(".panel").length,
        0,
        "Answer was added"     
      )

      equal(
        find('.panel-body').text().replace(/\s+/g,''),
        'Answer',
        "Answer was rendered"
      );

    });
  });
});