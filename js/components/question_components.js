App.QuestionPreviewComponent = Ember.Component.extend({
	tagName: 'li',

	answerCount: Ember.computed.alias('question.answers.length'),

	pluralForm: function(){
		var answers = this.get('answerCount');
		return answers === 1 ? 'answer' : 'answers';
	}.property('answerCount'),

	answersPresent: function(){
		return this.get('answerCount') > 0;
	}.property('answerCount')
});