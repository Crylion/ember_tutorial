App.SetAuthorMixin = Ember.Mixin.create({
	needs: ['application'],

	setAuthorFor: function(object){
		this.get('controllers.application.signedInUser').then(function(user){
			object.set('author', user);
		});
	}
});

App.AskQuestionController = Ember.ArrayController.extend(
App.SetAuthorMixin,{
	needs: ['application'],
	sortProperties: ['date'],
	sortAscending: true,

	latestQuestions: function(){
		return this.slice(0,3);
	}.property('@each'),

	actions: {
		askQuestion: function(){
			var question = this.store.createRecord('question', {
				topic:		this.get('topic'),
				question:	this.get('question'),
				date: 		new Date()
			});

			this.setAuthorFor(question);

			var controller = this;

			question.save().then(function(question){
				controller.setProperties({
					topic: 		'',
					question: 	''
				});
			});

			controller.transitionToRoute('question', question);
		}

		
	}
});

App.QuestionsController = Ember.ArrayController.extend({
	siteTitle: 'Welcome to Emberoverflow',

	currentDate: function() {
		return(new Date);
	}.property()

});

App.QuestionController = Ember.ObjectController.extend(
App.SetAuthorMixin, {
	needs: ['application'],
	isEditing: false,

	canEditQuestion: function(){
		return this.get('author.id') == App.currentUser;
	}.property('model'),

	actions:{
		toggleEditQuestion: function(){
			this.toggleProperty('isEditing');
		},

		submitEdit: function(){
			
			var controller = this;
			this.get('model').save().then(function(question){
				controller.toggleProperty('isEditing');
			});
		},

		answerQuestion: function(){
			var answer = this.store.createRecord('answer', {
				answer:		this.get('answer'),
				date: 		new Date()
			});

			this.setAuthorFor(answer);

			var controller = this;

			answer.save().then(function(answer){
				controller.get('model.answers').addObject(answer);
				controller.setProperties({
					answer: 		''
				});
			});
		}
	}

});