App.Question.FIXTURES=[
{
 id: 		101, 
 topic: 	"How to develop complex Apps with Ember", 
 author: 	201,
 date: 		'2013-01-01T12:00:00',
 question: 	"I'm wondering how you would go about creating complex applications in ember.js with such an opinionated framework"
},
{
 id: 		102, 
 topic: 	'Thinking Big: How do I focus my ideas', 
 author: 	202,
 date: 		'2013-01-02T14:00:00',
 question: 	'My product visions, which I wake up with every morning, are always quite revolutionizing. How do I get them back on a coherent track?',
 answers:	[301,302]
}];